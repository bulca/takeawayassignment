package com.selcukbulca.takeawayassignment.util

import com.google.gson.Gson
import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.data.model.RestaurantResponse
import com.selcukbulca.takeawayassignment.sort.SortOption
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.io.File

@RunWith(JUnit4::class)
class RestaurantSorterTest {

    private val gson = Gson()
    private lateinit var restaurants: MutableList<Restaurant>
    private lateinit var favoriteRestaurants: MutableList<Restaurant>
    private lateinit var restaurantsSortedByMinCost: MutableList<Restaurant>
    private lateinit var restaurantsSortedByDeliveryCosts: MutableList<Restaurant>
    private lateinit var restaurantsSortedByAverageProductPrice: MutableList<Restaurant>
    private lateinit var restaurantsSortedByPopularity: MutableList<Restaurant>
    private lateinit var restaurantsSortedByDistance: MutableList<Restaurant>
    private lateinit var restaurantsSortedByRatingAverage: MutableList<Restaurant>
    private lateinit var restaurantsSortedByNewest: MutableList<Restaurant>
    private lateinit var restaurantsSortedByBestMatch: MutableList<Restaurant>
    private lateinit var favoriteRestaurantsSortedByMinCost: MutableList<Restaurant>

    private fun parseRestaurants(fileName: String): MutableList<Restaurant> {
        val resource = javaClass.classLoader.getResource(fileName)
        val file = File(resource.path)
        return gson.fromJson(file.readText(), RestaurantResponse::class.java)
            .restaurants.toMutableList()
    }

    private fun testSort(
        restaurants: MutableList<Restaurant>,
        sortOption: SortOption,
        sortedRestaurants: MutableList<Restaurant>
    ) {
        RestaurantSorter.sort(restaurants, sortOption)
        assertThat(restaurants).isEqualTo(sortedRestaurants)
    }

    @Before
    fun setup() {
        restaurants = parseRestaurants("restaurants_not_sorted.json")
        restaurantsSortedByMinCost = parseRestaurants("restaurants_sorted_by_min_cost.json")
        restaurantsSortedByDeliveryCosts =
                parseRestaurants("restaurants_sorted_by_delivery_costs.json")
        restaurantsSortedByAverageProductPrice =
                parseRestaurants("restaurants_sorted_by_average_product_price.json")
        restaurantsSortedByPopularity = parseRestaurants("restaurants_sorted_by_popularity.json")
        restaurantsSortedByDistance = parseRestaurants("restaurants_sorted_by_distance.json")
        restaurantsSortedByRatingAverage =
                parseRestaurants("restaurants_sorted_by_rating_average.json")
        restaurantsSortedByNewest = parseRestaurants("restaurants_sorted_by_newest.json")
        restaurantsSortedByBestMatch = parseRestaurants("restaurants_sorted_by_best_match.json")

        favoriteRestaurants = parseRestaurants("favorite_restaurants_not_sorted.json")
        favoriteRestaurantsSortedByMinCost =
                parseRestaurants("favorite_restaurants_sorted_by_min_cost.json")
    }

    @Test
    fun testSortByMinCost() {
        testSort(restaurants, SortOption.MIN_COST, restaurantsSortedByMinCost)
    }

    @Test
    fun testSortByDeliveryCosts() {
        testSort(restaurants, SortOption.DELIVERY_COSTS, restaurantsSortedByDeliveryCosts)
    }

    @Test
    fun testSortByAverageProductPrice() {
        testSort(
            restaurants,
            SortOption.AVERAGE_PRODUCT_PRICE,
            restaurantsSortedByAverageProductPrice
        )
    }

    @Test
    fun testSortByPopularity() {
        testSort(restaurants, SortOption.POPULARITY, restaurantsSortedByPopularity)
    }

    @Test
    fun testSortByDistance() {
        testSort(restaurants, SortOption.DISTANCE, restaurantsSortedByDistance)
    }

    @Test
    fun testSortByRatingAverage() {
        testSort(restaurants, SortOption.RATING_AVERAGE, restaurantsSortedByRatingAverage)
    }

    @Test
    fun testSortByNewest() {
        testSort(restaurants, SortOption.NEWEST, restaurantsSortedByNewest)
    }

    @Test
    fun testSortByBestMatch() {
        testSort(restaurants, SortOption.BEST_MATCH, restaurantsSortedByBestMatch)
    }

    @Test
    fun testFavoriteRestaurantsSortedByMinCost() {
        testSort(favoriteRestaurants, SortOption.MIN_COST, favoriteRestaurantsSortedByMinCost)
    }
}