package com.selcukbulca.takeawayassignment.restaurants

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockito_kotlin.*
import com.selcukbulca.takeawayassignment.data.source.RestaurantsRepository
import com.selcukbulca.takeawayassignment.sort.SortOption
import com.selcukbulca.takeawayassignment.util.RxSchedulers
import io.reactivex.Flowable
import io.reactivex.schedulers.TestScheduler
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import java.net.UnknownHostException

@RunWith(JUnit4::class)
class RestaurantsViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    @Mock
    lateinit var restaurantsRepository: RestaurantsRepository

    private lateinit var restaurantsViewModel: RestaurantsViewModel
    private lateinit var testSchedulers: RxSchedulers
    private lateinit var testScheduler: TestScheduler

    @Before
    fun setup() {
        restaurantsRepository = mock {
            on { getRestaurants() } doReturn Flowable.just(RESTAURANTS_TEST_DATA)
        }
        testScheduler = TestScheduler()
        testSchedulers = RxSchedulers(testScheduler, testScheduler, testScheduler)
        restaurantsViewModel = RestaurantsViewModel(restaurantsRepository, testSchedulers)
    }

    @Test
    fun getRestaurantsCallsRepositoryOnce() {
        restaurantsViewModel.getRestaurants()

        verify(restaurantsRepository, times(1)).getRestaurants()
    }

    @Test
    fun getRestaurantsReturnsLoading() {
        restaurantsViewModel.getRestaurants()

        assert(restaurantsViewModel.viewState.value == RestaurantsViewState.Loading)
    }

    @Test
    fun getRestaurantsReturnsSuccess() {
        restaurantsViewModel.getRestaurants()
        testScheduler.triggerActions()

        assert(restaurantsViewModel.viewState.value is RestaurantsViewState.Success)

        val successViewState = restaurantsViewModel.viewState.value as RestaurantsViewState.Success

        assertThat(successViewState.restaurants).hasSize(3)
        assertThat(successViewState.restaurants).isEqualTo(RESTAURANTS_TEST_DATA)
    }

    @Test
    fun getRestaurantsReturnsError() {
        whenever(restaurantsRepository.getRestaurants()).thenReturn(
            Flowable.error(
                UnknownHostException()
            )
        )

        restaurantsViewModel.getRestaurants()
        testScheduler.triggerActions()

        assertThat(restaurantsViewModel.viewState.value is RestaurantsViewState.Error)

        val errorViewState = restaurantsViewModel.viewState.value as RestaurantsViewState.Error
        assertThat(errorViewState.throwable is UnknownHostException)
    }

    @Test
    fun testSearchRestaurants() {
        restaurantsViewModel.getRestaurants()
        testScheduler.triggerActions()

        restaurantsViewModel.search("Open")

        assertThat(restaurantsViewModel.viewState.value is RestaurantsViewState.Success)

        val successViewState = restaurantsViewModel.viewState.value as RestaurantsViewState.Success
        assertThat(successViewState.restaurants).hasSize(1)
        assertThat(successViewState.filter).isNotNull()
        assertThat(successViewState.filter!!.query).isEqualTo("Open")
    }

    @Test
    fun testSortRestaurants() {
        restaurantsViewModel.getRestaurants()
        testScheduler.triggerActions()

        restaurantsViewModel.sort(SortOption.MIN_COST)

        assertThat(restaurantsViewModel.viewState.value is RestaurantsViewState.Success)

        val successViewState = restaurantsViewModel.viewState.value as RestaurantsViewState.Success
        assertThat(successViewState.restaurants).hasSize(3)
        assertThat(successViewState.filter).isNotNull()
        assertThat(successViewState.filter!!.sortOption).isEqualTo(SortOption.MIN_COST)
        assertThat(successViewState.restaurants).isEqualTo(RESTAURANTS_TEST_DATA_SORTED_BY_MIN_COST)
    }
}