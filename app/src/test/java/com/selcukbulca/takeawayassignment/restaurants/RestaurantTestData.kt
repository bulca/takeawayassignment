package com.selcukbulca.takeawayassignment.restaurants

import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.data.model.RestaurantStatus
import com.selcukbulca.takeawayassignment.data.model.SortingValues

val RESTAURANT_CLOSED = Restaurant(
    "Closed test restaurant",
    RestaurantStatus.CLOSED,
    false,
    SortingValues(0, 0, 0, 0, 0, 0.0, 0, 0)
)

val RESTAURANT_OPEN = Restaurant(
    "Open test restaurant",
    RestaurantStatus.OPEN,
    false,
    SortingValues(1, 0, 0, 0, 0, 0.0, 0, 0)
)

val RESTAURANT_ORDER_AHEAD = Restaurant(
    "Order ahead test restaurant",
    RestaurantStatus.ORDER_AHEAD,
    false,
    SortingValues(2, 0, 0, 0, 0, 0.0, 0, 0)
)

val RESTAURANTS_TEST_DATA =
    listOf(RESTAURANT_CLOSED, RESTAURANT_OPEN, RESTAURANT_ORDER_AHEAD)
val RESTAURANTS_TEST_DATA_SORTED_BY_MIN_COST =
    listOf(RESTAURANT_OPEN, RESTAURANT_ORDER_AHEAD, RESTAURANT_CLOSED)