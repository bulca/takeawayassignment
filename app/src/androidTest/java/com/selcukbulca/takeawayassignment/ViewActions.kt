package com.selcukbulca.takeawayassignment

import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.view.View
import org.hamcrest.Matcher

fun clickChildViewWithId(id: Int): ViewAction {
    return object : ViewAction {
        override fun getConstraints(): Matcher<View>? {
            return null
        }

        override fun getDescription(): String {
            return "click on a child view with specified id"
        }

        override fun perform(uiController: UiController, view: View) {
            val childView = view.findViewById<View>(id)
            if (childView != null) {
                childView.performClick()
            }
        }
    }
}