package com.selcukbulca.takeawayassignment.restaurants

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeTextIntoFocusedView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.RootMatchers.isPlatformPopup
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.selcukbulca.takeawayassignment.R
import com.selcukbulca.takeawayassignment.clickChildViewWithId
import org.hamcrest.core.StringContains.containsString
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RestaurantsActivityTest {

    companion object {
        const val DELAY_MS = 2000L
    }

    @get:Rule
    val activityTestRule = ActivityTestRule(RestaurantsActivity::class.java)

    private fun delay() {
        Thread.sleep(DELAY_MS)
    }

    @Before
    fun setup() {
        activityTestRule.launchActivity(Intent())
    }

    @Test
    fun checksProgressGroupIsDisplayed() {
        onView(withId(R.id.loadingProgressBar)).check(matches(isDisplayed()))
        onView(withId(R.id.loadingTextView)).check(matches(isDisplayed()))
    }

    @Test
    fun checkRestaurantListIsDisplayed() {
        delay()
        onView(withId(R.id.restaurantList)).check(matches(isDisplayed()))
    }

    @Test
    fun checkSearchAndFavoriteRestaurants() {
        delay()
        onView(withId(R.id.search_restaurants)).perform(click())
        onView(withId(R.id.search_src_text)).perform(typeTextIntoFocusedView("Tanoshii"))

        delay()
        onView(withId(R.id.restaurantList)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RestaurantViewHolder>(
                0, clickChildViewWithId(R.id.favorite)
            )
        )
    }

    @Test
    fun checkSortOptionsIsDisplayed() {
        delay()
        onView(withId(R.id.sortFab)).perform(click())

        onView(withId(R.id.sortOptionsSpinner)).check(matches(isDisplayed()))
        onView(withId(R.id.applyButton)).check(matches(isDisplayed()))
    }

    @Test
    fun checkSortOptionChangedViaSpinner() {
        delay()
        onView(withId(R.id.sortFab)).perform(click())

        onView(withId(R.id.sortOptionsSpinner)).perform(click())
        onView(withText("Newest")).inRoot(isPlatformPopup()).perform(click())
        onView(withId(R.id.applyButton)).perform(click())

        onView(withId(R.id.selectedSortOptionTextView))
            .check(matches(withText(containsString("Newest"))))
    }
}