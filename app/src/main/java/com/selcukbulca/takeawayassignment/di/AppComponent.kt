package com.selcukbulca.takeawayassignment.di

import com.selcukbulca.takeawayassignment.restaurants.RestaurantsActivity
import com.selcukbulca.takeawayassignment.restaurants.RestaurantsModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        DbModule::class,
        RestaurantsModule::class
    ]
)
interface AppComponent {
    fun inject(restaurantsActivity: RestaurantsActivity)
}