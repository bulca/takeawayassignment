package com.selcukbulca.takeawayassignment.di

import com.selcukbulca.takeawayassignment.BuildConfig
import dagger.Module
import dagger.Provides
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    companion object {
        const val NAME_RESTAURANTS_ENDPOINT = "restaurants-endpoint"
    }

    @Provides
    @Named(NAME_RESTAURANTS_ENDPOINT)
    fun provideRestaurantsEndpoint(): String {
        return BuildConfig.RESTAURANTS_ENDPOINT
    }

    @Provides
    @Singleton
    fun provideCallAdapterFactory(): CallAdapter.Factory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    @Singleton
    fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Singleton
    @Provides
    fun provideRetrofit(
        @Named(NAME_RESTAURANTS_ENDPOINT) endpoint: String,
        callAdapter: CallAdapter.Factory,
        converterFactory: Converter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(endpoint)
            .addCallAdapterFactory(callAdapter)
            .addConverterFactory(converterFactory)
            .build()
    }
}