package com.selcukbulca.takeawayassignment.di

import android.arch.persistence.room.Room
import android.content.Context
import com.selcukbulca.takeawayassignment.data.source.local.RestaurantDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Provides
    @Singleton
    fun provideRestaurantsDatabase(context: Context) =
        Room.databaseBuilder(context, RestaurantDatabase::class.java, "restaurants-db").build()
}