package com.selcukbulca.takeawayassignment.di

import android.app.Application
import android.content.Context
import com.selcukbulca.takeawayassignment.util.RxSchedulers
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {
    
    @Provides
    fun provideContext(): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    fun provideRxSchedulers(): RxSchedulers {
        return RxSchedulers(
            Schedulers.single(),
            Schedulers.io(),
            AndroidSchedulers.mainThread()
        )
    }
}