package com.selcukbulca.takeawayassignment

import android.app.Application
import com.selcukbulca.takeawayassignment.di.AppComponent
import com.selcukbulca.takeawayassignment.di.AppModule
import com.selcukbulca.takeawayassignment.di.DaggerAppComponent
import timber.log.Timber

class TakeAwayApp : Application() {

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}