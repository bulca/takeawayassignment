package com.selcukbulca.takeawayassignment.comparator

import com.selcukbulca.takeawayassignment.data.model.Restaurant
import kotlin.math.sign

object RatingAverageComparator : Comparator<Restaurant> {
    override fun compare(first: Restaurant, second: Restaurant): Int {
        return baseCompare(first, second) {
            sign(second.sortingValues.ratingAverage - first.sortingValues.ratingAverage).toInt()
        }
    }
}