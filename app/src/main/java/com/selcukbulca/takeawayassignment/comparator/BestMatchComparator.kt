package com.selcukbulca.takeawayassignment.comparator

import com.selcukbulca.takeawayassignment.data.model.Restaurant

object BestMatchComparator : Comparator<Restaurant> {
    override fun compare(first: Restaurant, second: Restaurant): Int {
        return baseCompare(first, second) {
            second.sortingValues.bestMatch - first.sortingValues.bestMatch
        }
    }
}