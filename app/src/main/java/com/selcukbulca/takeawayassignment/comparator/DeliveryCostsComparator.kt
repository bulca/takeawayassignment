package com.selcukbulca.takeawayassignment.comparator

import com.selcukbulca.takeawayassignment.data.model.Restaurant

object DeliveryCostsComparator : Comparator<Restaurant> {
    override fun compare(first: Restaurant, second: Restaurant): Int {
        return baseCompare(first, second) {
            first.sortingValues.deliveryCosts - second.sortingValues.deliveryCosts
        }
    }
}