package com.selcukbulca.takeawayassignment.comparator

import com.selcukbulca.takeawayassignment.data.model.Restaurant

object AverageProductPriceComparator : Comparator<Restaurant> {
    override fun compare(first: Restaurant, second: Restaurant): Int {
        return baseCompare(first, second) {
            first.sortingValues.averageProductPrice - second.sortingValues.averageProductPrice
        }
    }
}