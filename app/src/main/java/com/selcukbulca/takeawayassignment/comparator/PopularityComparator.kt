package com.selcukbulca.takeawayassignment.comparator

import com.selcukbulca.takeawayassignment.data.model.Restaurant

object PopularityComparator : Comparator<Restaurant> {
    override fun compare(first: Restaurant, second: Restaurant): Int {
        return baseCompare(first, second) {
            second.sortingValues.popularity - first.sortingValues.popularity
        }
    }
}