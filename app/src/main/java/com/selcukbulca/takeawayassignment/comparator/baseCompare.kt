package com.selcukbulca.takeawayassignment.comparator

import com.selcukbulca.takeawayassignment.data.model.Restaurant

internal inline fun baseCompare(
    first: Restaurant,
    second: Restaurant,
    compareFunc: () -> Int
): Int {
    if (first.favorite && second.favorite) {
        return if (first.status.value == second.status.value) {
            compareFunc.invoke()
        } else {
            second.status.value - first.status.value
        }
    } else if (first.favorite) {
        return -1
    } else if (second.favorite) {
        return 1
    } else {
        return if (first.status.value == second.status.value) {
            compareFunc.invoke()
        } else {
            second.status.value - first.status.value
        }
    }
}