package com.selcukbulca.takeawayassignment.ext

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat

fun Context.drawable(@DrawableRes drawableResId: Int): Drawable =
    ContextCompat.getDrawable(this, drawableResId)!!