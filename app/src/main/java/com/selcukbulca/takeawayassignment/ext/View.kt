package com.selcukbulca.takeawayassignment.ext

import android.view.MenuItem
import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isVisible

fun View.hide() {
    if (!this.isGone) {
        visibility = View.GONE
    }
}

fun View.show() {
    if (!this.isVisible) {
        visibility = View.VISIBLE
    }
}

fun MenuItem.show() {
    isVisible = true
}

fun MenuItem.hide() {
    isVisible = false
}