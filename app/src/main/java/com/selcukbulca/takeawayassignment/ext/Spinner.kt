package com.selcukbulca.takeawayassignment.ext

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner

fun Spinner.setupWith(
    items: List<String>,
    selectedIndex: Int = 0
) {
    this.adapter = ArrayAdapter<String>(
        context,
        android.R.layout.simple_spinner_item,
        items
    ).apply {
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    }

    // Index zero is already selected by default
    if (selectedIndex != 0) {
        this.setSelection(selectedIndex)
    }
}

fun Spinner.onItemSelected(onItemSelected: (position: Int) -> Unit) {
    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>) {
            // no-op
        }

        override fun onItemSelected(
            parent: AdapterView<*>,
            view: View,
            position: Int,
            id: Long
        ) {
            onItemSelected.invoke(position)
        }
    }
}