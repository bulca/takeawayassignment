package com.selcukbulca.takeawayassignment.sort

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.selcukbulca.takeawayassignment.R
import com.selcukbulca.takeawayassignment.ext.get
import com.selcukbulca.takeawayassignment.ext.onItemSelected
import com.selcukbulca.takeawayassignment.ext.setupWith
import com.selcukbulca.takeawayassignment.restaurants.RestaurantsActivity
import com.selcukbulca.takeawayassignment.restaurants.RestaurantsViewModel
import kotlinx.android.synthetic.main.fragment_sort_options.*

class SortOptionsFragment : BottomSheetDialogFragment() {

    companion object {
        private const val TAG = "SortOptionsFragment"
        private const val SELECTED_SORT_OPTION = "selected_sort_option"

        private fun newInstance(selectedSortOption: SortOption): SortOptionsFragment {
            return SortOptionsFragment().apply {
                arguments = Bundle().apply {
                    putInt(SELECTED_SORT_OPTION, selectedSortOption.ordinal)
                }
            }
        }

        fun show(activity: AppCompatActivity, selectedSortOption: SortOption) {
            newInstance(selectedSortOption).show(activity.supportFragmentManager, TAG)
        }
    }

    private lateinit var viewModel: RestaurantsViewModel
    private lateinit var selectedSortOption: SortOption

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val activity = requireActivity() as RestaurantsActivity
        viewModel = ViewModelProviders.of(activity, activity.viewModelFactory).get()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sort_options, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val selectedSortOptionIndex = arguments?.getInt(SELECTED_SORT_OPTION, 0) ?: 0
        val sortOptions = SortOption.values()
        selectedSortOption = sortOptions[selectedSortOptionIndex]
        addSortOptionsToSpinner(sortOptions, selectedSortOptionIndex)

        applyButton.setOnClickListener {
            viewModel.sort(selectedSortOption)
            dismiss()
        }
    }

    private fun addSortOptionsToSpinner(
        sortOptions: Array<SortOption>,
        selectedIndex: Int
    ) {
        val sortOptionNames = sortOptions.map { it.description }.toList()
        sortOptionsSpinner.apply {
            setupWith(sortOptionNames, selectedIndex)
            onItemSelected { position ->
                this@SortOptionsFragment.selectedSortOption = sortOptions[position]
            }
        }
    }
}