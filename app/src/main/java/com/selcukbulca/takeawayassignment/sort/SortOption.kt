package com.selcukbulca.takeawayassignment.sort

enum class SortOption(val description: String) {
    MIN_COST("Minimum Cost"),
    DELIVERY_COSTS("Delivery Costs"),
    AVERAGE_PRODUCT_PRICE("Average Product Price"),
    POPULARITY("Popularity"),
    DISTANCE("Distance"),
    RATING_AVERAGE("Rating Average"),
    NEWEST("Newest"),
    BEST_MATCH("Best Match")
}