package com.selcukbulca.takeawayassignment.util

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.selcukbulca.takeawayassignment.data.source.RestaurantsRepository
import com.selcukbulca.takeawayassignment.restaurants.RestaurantsViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ViewModelFactory @Inject constructor(
    private val restaurantsRepository: RestaurantsRepository,
    private val schedulers: RxSchedulers
) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RestaurantsViewModel::class.java)) {
            return RestaurantsViewModel(restaurantsRepository, schedulers) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}