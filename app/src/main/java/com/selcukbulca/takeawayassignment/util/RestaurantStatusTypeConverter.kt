package com.selcukbulca.takeawayassignment.util

import android.arch.persistence.room.TypeConverter
import com.selcukbulca.takeawayassignment.data.model.RestaurantStatus

object RestaurantStatusTypeConverter {

    @TypeConverter
    @JvmStatic
    fun toStatus(ordinal: Int): RestaurantStatus {
        return RestaurantStatus.VALUES[ordinal]
    }

    @TypeConverter
    @JvmStatic
    fun toInt(status: RestaurantStatus): Int {
        return status.ordinal
    }
}