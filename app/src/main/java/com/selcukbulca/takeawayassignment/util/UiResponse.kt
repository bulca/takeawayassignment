package com.selcukbulca.takeawayassignment.util

sealed class UiResponse {
    object Loading : UiResponse()
    class Error(val message: String) : UiResponse()
    class Success<T>(val response: T) : UiResponse()
}