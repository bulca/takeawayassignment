package com.selcukbulca.takeawayassignment.util

import io.reactivex.Scheduler

class RxSchedulers(
    val database: Scheduler,
    val network: Scheduler,
    val main: Scheduler
)