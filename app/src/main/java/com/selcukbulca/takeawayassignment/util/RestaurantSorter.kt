package com.selcukbulca.takeawayassignment.util

import com.selcukbulca.takeawayassignment.comparator.*
import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.sort.SortOption

object RestaurantSorter {
    fun sort(restaurants: MutableList<Restaurant>, sortOption: SortOption) {
        when (sortOption) {
            SortOption.MIN_COST -> MinCostComparator
            SortOption.DELIVERY_COSTS -> DeliveryCostsComparator
            SortOption.AVERAGE_PRODUCT_PRICE -> AverageProductPriceComparator
            SortOption.POPULARITY -> PopularityComparator
            SortOption.DISTANCE -> DistanceComparator
            SortOption.RATING_AVERAGE -> RatingAverageComparator
            SortOption.NEWEST -> NewestComparator
            SortOption.BEST_MATCH -> BestMatchComparator
        }.apply {
            restaurants.sortWith(this)
        }
    }
}