package com.selcukbulca.takeawayassignment.util

import android.support.v7.util.DiffUtil
import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.sort.SortOption

class RestaurantsDiffCallback(
    private val oldRestaurants: List<Restaurant>,
    private val newRestaurants: List<Restaurant>,
    private val oldSortOption: SortOption?,
    private val newSortOption: SortOption?
) : DiffUtil.Callback() {

    override fun getOldListSize() =
        oldRestaurants.size

    override fun getNewListSize() =
        newRestaurants.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
    // Lets use the restaurant name, since we don't have an id
        oldRestaurants[oldItemPosition].name == newRestaurants[newItemPosition].name

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        newSortOption == oldSortOption
                && oldRestaurants[oldItemPosition] == newRestaurants[newItemPosition]

}