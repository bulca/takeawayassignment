package com.selcukbulca.takeawayassignment.restaurants

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.text.Html
import android.view.Menu
import android.view.MenuItem
import com.github.ajalt.timberkt.e
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import com.selcukbulca.takeawayassignment.R
import com.selcukbulca.takeawayassignment.TakeAwayApp
import com.selcukbulca.takeawayassignment.ext.get
import com.selcukbulca.takeawayassignment.ext.hide
import com.selcukbulca.takeawayassignment.ext.show
import com.selcukbulca.takeawayassignment.sort.SortOption
import com.selcukbulca.takeawayassignment.sort.SortOptionsFragment
import com.selcukbulca.takeawayassignment.util.RxSchedulers
import com.selcukbulca.takeawayassignment.util.ViewModelFactory
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_restaurants.*
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RestaurantsActivity : AppCompatActivity() {

    companion object {
        val DEFAULT_SORT_OPTION = SortOption.MIN_COST
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    @Inject
    lateinit var schedulers: RxSchedulers

    private lateinit var viewModel: RestaurantsViewModel
    private var searchMenuItem: MenuItem? = null
    private var searchDisposable: Disposable? = null
    private var selectedSortOption = DEFAULT_SORT_OPTION
    private val restaurantAdapter = RestaurantAdapter().apply {
        onFavoriteClicked = { viewModel.favorite(it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurants)
        injectDependencies()

        setupRecyclerView()

        viewModel = ViewModelProviders.of(this, viewModelFactory).get()
        viewModel.viewState.observe(this, Observer { viewState ->
            viewState?.let { render(it) }
        })
        viewModel.sortOptionChangeEvent.observe(this, Observer {
            restaurantList.scrollToPosition(0)
        })

        // Do not fetch restaurants again if the activity is recreated
        if (savedInstanceState == null) {
            viewModel.getRestaurants()
        }

        sortFab.setOnClickListener {
            SortOptionsFragment.show(this, selectedSortOption)
        }
        retryButton.setOnClickListener {
            viewModel.getRestaurants()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        searchDisposable?.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_restaurants, menu)
        setupSearchView(menu)
        return true
    }

    private fun injectDependencies() {
        (application as TakeAwayApp).appComponent.inject(this)
    }

    private fun setupRecyclerView() {
        val itemDecoration = DividerItemDecoration(this, LinearLayoutManager.HORIZONTAL)
        val layoutManager = LinearLayoutManager(this)
        restaurantList.apply {
            setHasFixedSize(true)
            addItemDecoration(itemDecoration)
            this.layoutManager = layoutManager
            this.adapter = restaurantAdapter
        }
    }

    private fun setupSearchView(menu: Menu) {
        searchMenuItem = menu.findItem(R.id.search_restaurants)
        val searchView = searchMenuItem!!.actionView as SearchView

        searchDisposable = RxSearchView.queryTextChanges(searchView)
            .skipInitialValue()
            .debounce(400, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .observeOn(schedulers.main)
            .subscribeBy(onNext = { query ->
                viewModel.search(query.toString())
            })
    }

    private fun render(viewState: RestaurantsViewState) {
        when (viewState) {
            is RestaurantsViewState.Loading -> renderLoading()
            is RestaurantsViewState.Error -> renderError(viewState)
            is RestaurantsViewState.Success -> renderSuccess(viewState)
        }
    }

    private fun renderLoading() {
        loadingGroup.show()
        errorGroup.hide()
        contentGroup.hide()
        searchMenuItem?.show()
        restaurantAdapter.data = RestaurantAdapterData(mutableListOf(), null)
    }

    private fun renderError(viewState: RestaurantsViewState.Error) {
        loadingGroup.hide()
        errorGroup.show()
        contentGroup.hide()
        searchMenuItem?.hide()

        errorTextView.text = when (viewState.throwable) {
            is UnknownHostException -> getString(R.string.error_unknown_host)
            else -> getString(R.string.error_generic)
        }

        e(viewState.throwable) { "Error while getting restaurants" }
    }

    private fun renderSuccess(viewState: RestaurantsViewState.Success) {
        loadingGroup.hide()
        errorGroup.hide()
        contentGroup.show()
        searchMenuItem?.show()

        viewState.filter?.let {
            if (it.sortOption != null) {
                selectedSortOption = it.sortOption
                selectedSortOptionTextView.show()
                val selectedSortOptionText = getString(
                    R.string.selected_sort_option,
                    it.sortOption.description
                )
                @Suppress("DEPRECATION")
                selectedSortOptionTextView.text = Html.fromHtml(selectedSortOptionText)
            }
        }

        restaurantAdapter.data = restaurantAdapter.data.copy(
            restaurants = viewState.restaurants,
            sortOption = viewState.filter?.sortOption
        )
    }
}
