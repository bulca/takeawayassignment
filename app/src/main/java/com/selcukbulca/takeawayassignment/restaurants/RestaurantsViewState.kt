package com.selcukbulca.takeawayassignment.restaurants

import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.sort.SortOption

sealed class RestaurantsViewState {
    object Loading : RestaurantsViewState()
    class Error(val throwable: Throwable) : RestaurantsViewState()
    class Success(val restaurants: List<Restaurant>, val filter: Filter? = null) :
        RestaurantsViewState()
}

class Filter(val sortOption: SortOption?, val query: String?)