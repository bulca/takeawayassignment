package com.selcukbulca.takeawayassignment.restaurants

import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.sort.SortOption

data class RestaurantAdapterData(val restaurants: List<Restaurant>, val sortOption: SortOption?)