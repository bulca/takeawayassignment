package com.selcukbulca.takeawayassignment.restaurants

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.selcukbulca.takeawayassignment.R
import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.util.RestaurantsDiffCallback

class RestaurantAdapter : RecyclerView.Adapter<RestaurantViewHolder>() {

    var data: RestaurantAdapterData = RestaurantAdapterData(emptyList(), null)
        set(value) {
            DiffUtil.calculateDiff(
                RestaurantsDiffCallback(
                    field.restaurants,
                    value.restaurants,
                    field.sortOption,
                    value.sortOption
                )
            ).dispatchUpdatesTo(this)
            field = value
        }

    var onFavoriteClicked: ((Restaurant) -> Unit)? = null

    override fun getItemCount(): Int =
        data.restaurants.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_restaurant, parent, false)
        return RestaurantViewHolder(view, onFavoriteClicked)
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        holder.bind(data.restaurants[position], data.sortOption)
    }
}