package com.selcukbulca.takeawayassignment.restaurants

import android.support.v7.widget.RecyclerView
import android.view.View
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import com.selcukbulca.takeawayassignment.R
import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.sort.SortOption
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_restaurant.view.*

class RestaurantViewHolder(
    override val containerView: View,
    private val onFavoriteClicked: ((Restaurant) -> Unit)?
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(restaurant: Restaurant, sortOption: SortOption?) {
        containerView.apply {
            name.text = restaurant.name
            details.text = buildSpannedString {
                append(restaurant.status.description)
                if (sortOption != null) {
                    append(" • ")
                    bold { append(restaurant.getSortOptionValue(sortOption)) }
                }
            }

            if (restaurant.favorite) {
                favorite.setImageResource(R.drawable.ic_star)
            } else {
                favorite.setImageResource(R.drawable.ic_star_border)
            }

            favorite.setOnClickListener { onFavoriteClicked?.invoke(restaurant) }
        }
    }
}