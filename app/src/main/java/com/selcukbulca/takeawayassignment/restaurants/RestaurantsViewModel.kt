package com.selcukbulca.takeawayassignment.restaurants

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.data.source.RestaurantsRepository
import com.selcukbulca.takeawayassignment.sort.SortOption
import com.selcukbulca.takeawayassignment.util.RestaurantSorter
import com.selcukbulca.takeawayassignment.util.RxSchedulers
import com.selcukbulca.takeawayassignment.util.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy

class RestaurantsViewModel(
    private val restaurantsRepository: RestaurantsRepository,
    private val schedulers: RxSchedulers
) : ViewModel() {

    val viewState: MutableLiveData<RestaurantsViewState> = MutableLiveData()
    val sortOptionChangeEvent: SingleLiveEvent<Unit> = SingleLiveEvent()

    private lateinit var restaurants: List<Restaurant>
    private var filter: Filter? = null
    private val disposables = CompositeDisposable()

    fun getRestaurants() {
        disposables += restaurantsRepository.getRestaurants()
            .subscribeOn(schedulers.network)
            .observeOn(schedulers.main)
            .doOnSubscribe({ viewState.value = RestaurantsViewState.Loading })
            .subscribeBy(onNext = { restaurants ->
                this.restaurants = restaurants
                applyFilter(filter)
            }, onError = { throwable ->
                viewState.value = RestaurantsViewState.Error(throwable)
            })
    }

    fun sort(sortOption: SortOption) {
        filter = Filter(sortOption, filter?.query)
        applyFilter(filter)
        sortOptionChangeEvent.value = Unit
    }

    private fun sort(restaurants: MutableList<Restaurant>, sortOption: SortOption) {
        RestaurantSorter.sort(restaurants, sortOption)
    }

    fun search(query: String) {
        filter = Filter(filter?.sortOption, query)
        applyFilter(filter)
    }

    private fun search(restaurants: MutableList<Restaurant>, query: String) {
        for (restaurant in restaurants.toList()) {
            if (!restaurant.name.contains(query, ignoreCase = true)) {
                restaurants.remove(restaurant)
            }
        }
    }

    private fun applyFilter(filter: Filter?) {
        val mutableRestaurants = restaurants.toMutableList()
        if (filter != null) {
            if (filter.query != null) {
                search(mutableRestaurants, filter.query)
            }
            if (filter.sortOption != null) {
                sort(mutableRestaurants, filter.sortOption)
            }
        }

        viewState.value = RestaurantsViewState.Success(mutableRestaurants, filter)
    }

    fun favorite(restaurant: Restaurant) {
        disposables += restaurantsRepository.update(restaurant.copy(favorite = !restaurant.favorite))
            .subscribeOn(schedulers.database)
            .subscribe()
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}