package com.selcukbulca.takeawayassignment.restaurants

import com.selcukbulca.takeawayassignment.data.source.remote.RestaurantsService
import com.selcukbulca.takeawayassignment.ext.create
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class RestaurantsModule {
    @Provides
    @Singleton
    fun provideRestaurantsService(retrofit: Retrofit): RestaurantsService {
        return retrofit.create()
    }
}