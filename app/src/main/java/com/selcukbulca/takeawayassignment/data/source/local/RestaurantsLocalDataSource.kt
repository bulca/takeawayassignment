package com.selcukbulca.takeawayassignment.data.source.local

import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.data.source.RestaurantsDataSource
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class RestaurantsLocalDataSource @Inject constructor(
    restaurantsDatabase: RestaurantDatabase
) : RestaurantsDataSource {

    private val restaurantDao = restaurantsDatabase.restaurantDao()

    override fun getRestaurants(): Flowable<List<Restaurant>> {
        return restaurantDao.getAll()
    }

    fun save(restaurants: List<Restaurant>): Flowable<List<Restaurant>> {
        return Flowable.fromCallable {
            val savedRestaurants = restaurantDao.getAll().blockingFirst()
            if (savedRestaurants.isEmpty()) {
                restaurantDao.insertAll(restaurants)
            } else {
                // Set favorites from local db
                for (savedRestaurant in savedRestaurants) {
                    for (restaurant in restaurants) {
                        if (savedRestaurant == restaurant) {
                            restaurant.favorite = savedRestaurant.favorite
                        }
                    }
                }
            }
            return@fromCallable restaurants
        }
    }

    fun update(restaurant: Restaurant): Completable {
        return Completable.fromCallable {
            restaurantDao.update(restaurant)
        }
    }
}