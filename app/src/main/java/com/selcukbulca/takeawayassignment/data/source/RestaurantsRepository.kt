package com.selcukbulca.takeawayassignment.data.source

import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.data.source.local.RestaurantsLocalDataSource
import com.selcukbulca.takeawayassignment.data.source.remote.RestaurantsRemoteDataSource
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class RestaurantsRepository @Inject constructor(
    private val localDataSource: RestaurantsLocalDataSource,
    private val remoteDataSource: RestaurantsRemoteDataSource
) : RestaurantsDataSource {

    override fun getRestaurants(): Flowable<List<Restaurant>> {
        return remoteDataSource.getRestaurants()
            .flatMap { localDataSource.save(it) }
            .flatMap { localDataSource.getRestaurants() }
    }

    fun update(restaurant: Restaurant): Completable {
        return localDataSource.update(restaurant)
    }
}