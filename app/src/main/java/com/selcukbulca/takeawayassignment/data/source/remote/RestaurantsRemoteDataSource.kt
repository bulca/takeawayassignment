package com.selcukbulca.takeawayassignment.data.source.remote

import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.data.source.RestaurantsDataSource
import io.reactivex.Flowable
import javax.inject.Inject

class RestaurantsRemoteDataSource @Inject constructor(private val restaurantsService: RestaurantsService) :
    RestaurantsDataSource {
    override fun getRestaurants(): Flowable<List<Restaurant>> {
        return restaurantsService.fetchRestaurantResponse()
            .map { it.restaurants }
    }
}