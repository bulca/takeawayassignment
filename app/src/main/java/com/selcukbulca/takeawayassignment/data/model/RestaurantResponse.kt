package com.selcukbulca.takeawayassignment.data.model

class RestaurantResponse(val restaurants: List<Restaurant>)