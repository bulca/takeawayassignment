package com.selcukbulca.takeawayassignment.data.model

data class SortingValues(
    val minCost: Int,
    val deliveryCosts: Int,
    val averageProductPrice: Int,
    val popularity: Int,
    val distance: Int,
    val ratingAverage: Double,
    val newest: Int,
    val bestMatch: Int
)