package com.selcukbulca.takeawayassignment.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.selcukbulca.takeawayassignment.data.model.Restaurant
import com.selcukbulca.takeawayassignment.util.RestaurantStatusTypeConverter

@Database(entities = [(Restaurant::class)], version = 1)
@TypeConverters(RestaurantStatusTypeConverter::class)
abstract class RestaurantDatabase : RoomDatabase() {
    abstract fun restaurantDao(): RestaurantDao
}