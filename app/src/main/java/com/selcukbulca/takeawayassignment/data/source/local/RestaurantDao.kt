package com.selcukbulca.takeawayassignment.data.source.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import com.selcukbulca.takeawayassignment.data.model.Restaurant
import io.reactivex.Flowable

@Dao
interface RestaurantDao {
    @Query("SELECT * FROM restaurants")
    fun getAll(): Flowable<List<Restaurant>>

    @Insert(onConflict = REPLACE)
    fun insertAll(restaurants: List<Restaurant>)

    @Update(onConflict = REPLACE)
    fun update(restaurant: Restaurant)
}
