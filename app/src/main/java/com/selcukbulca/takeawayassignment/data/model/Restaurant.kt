package com.selcukbulca.takeawayassignment.data.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.selcukbulca.takeawayassignment.sort.SortOption

@Entity(tableName = "restaurants")
data class Restaurant(
    // Primary key should be something like Restaurant id, since we don't have it
    // let's use name as a primary key instead.
    @PrimaryKey val name: String,
    val status: RestaurantStatus,
    var favorite: Boolean,
    @Embedded val sortingValues: SortingValues
) {
    fun getSortOptionValue(sortOption: SortOption): String {
        return when (sortOption) {
            SortOption.MIN_COST -> sortingValues.minCost.toString()
            SortOption.DELIVERY_COSTS -> sortingValues.deliveryCosts.toString()
            SortOption.AVERAGE_PRODUCT_PRICE -> sortingValues.averageProductPrice.toString()
            SortOption.POPULARITY -> sortingValues.popularity.toString()
            SortOption.DISTANCE -> sortingValues.distance.toString()
            SortOption.RATING_AVERAGE -> sortingValues.ratingAverage.toString()
            SortOption.NEWEST -> sortingValues.newest.toString()
            SortOption.BEST_MATCH -> sortingValues.bestMatch.toString()
        }
    }
}