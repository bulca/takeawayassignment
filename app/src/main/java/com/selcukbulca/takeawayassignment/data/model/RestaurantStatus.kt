package com.selcukbulca.takeawayassignment.data.model

import com.google.gson.annotations.SerializedName

enum class RestaurantStatus(val value: Int, val description: String) {
    @SerializedName("closed")
    CLOSED(0, "Closed"),
    @SerializedName("order ahead")
    ORDER_AHEAD(1, "Order ahead"),
    @SerializedName("open")
    OPEN(2, "Open");

    companion object {
        val VALUES = values()
    }
}