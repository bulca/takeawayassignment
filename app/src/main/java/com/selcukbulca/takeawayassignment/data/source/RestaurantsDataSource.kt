package com.selcukbulca.takeawayassignment.data.source

import com.selcukbulca.takeawayassignment.data.model.Restaurant
import io.reactivex.Flowable

interface RestaurantsDataSource {
    fun getRestaurants(): Flowable<List<Restaurant>>
}