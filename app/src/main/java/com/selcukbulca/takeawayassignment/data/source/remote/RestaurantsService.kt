package com.selcukbulca.takeawayassignment.data.source.remote

import com.selcukbulca.takeawayassignment.data.model.RestaurantResponse
import io.reactivex.Flowable
import retrofit2.http.GET

interface RestaurantsService {
    @GET("5a44bb3e7835fe482d9cbfeb")
    fun fetchRestaurantResponse(): Flowable<RestaurantResponse>
}